# -*- coding: utf-8 -*-
from django.utils import timezone
from django.contrib import admin
#from django.contrib.admin.sites import AdminSite
from django.contrib.admin import AdminSite
from JayusApp.models import Miembro, Comunidad, Perfil, Denuncia
from JayusApp.models import Contenido, DetalleContenido, CustomUser
#from JayusApp.forms.userForm import CustomUserAdmin
#from ckeditor.widgets import CKEditorWidget
# Consultas
from JayusApp.consultas.consultasComunidad import esMiembro, obtener_por_user
from JayusApp.consultas.consultasComunidad import esLider
from JayusApp.consultas.consultasPerfil import ObtenerPerfil, HacerSuperUser
from JayusApp.Common.Enum import EstadoPerfilEnum


# Custom index on admin site
class MyAdminSite(AdminSite):
    index_template = 'admin/my_custom_index.html'
    app_index_template = 'admin/my_custom_app_index.html'

# Custom templates for abms
admin.ModelAdmin.change_form_template = 'admin/my_custom_change_form.html'
admin.ModelAdmin.change_list_template = 'admin/my_custom_change_list.html'
admin.ModelAdmin.delete_confirmation_template = 'admin/my_custom_delete_confirmation.html'
admin.ModelAdmin.delete_selected_confirmation_template = 'admin/my_custom_delete_selected.html'
admin.ModelAdmin.object_history_template = 'admin/my_custom_history.html'

# Custom admin site
admin_site = MyAdminSite(name='myadmin')
# Deshabilitar el delete que viene
admin_site.disable_action('delete_selected')


class AdminMiembro(admin.TabularInline):
    model = Miembro
    exclude = ['usuario_modificacion', 'fecha_modificacion', 'fecha_baja',
                'usuario_baja']
    extra = 0
    min_num = 0

    def delete_model(request, obj):
        obj.fecha_baja = timezone.now()
        obj.usuario_baja = request.user.username
        obj.save()

    def get_queryset(self, request):
        qs = super(AdminMiembro, self).get_queryset(request)
        return qs.filter(fecha_baja__isnull=True)



class AdminComunidad(admin.ModelAdmin):
    inlines = [
        AdminMiembro,
    ]
    list_display = ['nombre', 'descripcion']
    exclude = ['fecha_baja', 'usuario_baja']
    list_display_links = None

    # Modificar Queryset
    def get_queryset(self, request):
        qs = super(AdminComunidad, self).get_queryset(request)
        if request.user.is_admin:
            return qs
        return qs.filter(pk__in=obtener_por_user(request.user),
            fecha_baja__isnull=True)

    def has_change_permission(self, request, obj=None, **kwargs):
        # Si el objeto es none => true es la lista.
        if (obj is None):
            return True
        else:
            return False
            # Si está queriendo editar (Obj lleno)
                    #=> si es el propio si, sino no
            #if esMiembro(obj, request.user):
                #if esLider(obj, request.user):
                    #return True
                #return False
            #else:
                #return False

    def has_delete_permission(request, obj=None):
        # No deja crear denuncias
        return False

    def has_add_permission(request, obj=None):
        # No deja crear denuncias
        return False

    # Cuando guarda que lo suba en el rol
    def save_model(self, request, obj, form, change):
        perfil = ObtenerPerfil(request.user, request.get_host())
        HacerSuperUser(perfil, request.user)
        #import pdb; pdb.set_trace()
        obj.save()

    def eliminar_comunidad(self, request, queryset):
        queryset.update(fecha_baja=timezone.now,
            usuario_baja=request.user.username)
    eliminar_comunidad.short_description = "Eliminar comunidad seleccionada"

    actions = ['eliminar_comunidad']


class AdminPerfil(admin.ModelAdmin):
    list_display = ('usuario', 'nombre', 'apellido',
    'fechaNacimiento', 'sexo', 'estado')

    list_filter = ('sexo', 'estado')

    exclude = ['usuario', 'puntuacion', 'estado', 'groups']
    list_display_links = None

    ## Modificar Queryset
    def get_queryset(self, request):
        qs = super(AdminPerfil, self).get_queryset(request)
        return qs.exclude(estado=EstadoPerfilEnum.Baja.value)

    def has_add_permission(self, request, **kwargs):
        # No deja editar perfiles
        return False

    def has_change_permission(self, request, obj=None, **kwargs):
        # Si el objeto es none => true es la lista.
        if (obj is None):
            if (request.user.is_admin):
                return True
            return False
        else:
            return False
            # Si está queriendo editar (Obj lleno)
                    #=> si es el propio si, sino no
            #if request.user.id == obj.usuario.id:
                #return True
            #else:
                #return False

    def bloquear_perfil(self, request, queryset):
        queryset.update(estado=EstadoPerfilEnum.Bloqueado.value)
    bloquear_perfil.short_description = "Bloquear usuarios seleccionados"

    def eliminar_perfil(self, request, queryset):
        queryset.update(estado=EstadoPerfilEnum.Baja.value)
    eliminar_perfil.short_description = "Eliminar usuarios seleccionados"

    def habilitar_perfil(self, request, queryset):
        queryset.update(estado=EstadoPerfilEnum.Activo.value)
    habilitar_perfil.short_description = "Habilitar usuarios seleccionados"

    def hacer_admin_perfil(self, request, queryset):
        #TODO: funciona?
        for obj in queryset:
            user = CustomUser.objects.filter(id=obj.usuario.id)
            user.is_admin = True
            user.save()
    hacer_admin_perfil.short_description = "Hacerlos administradores"

    # Solo puede hacer las acciones el admin. Pero bueno entra solo el admin.
    actions = ['bloquear_perfil', 'eliminar_perfil', 'habilitar_perfil']


class AdminDenuncia(admin.ModelAdmin):
    #Custom field
    def descripcion_denuncia(self, obj):
        if (obj.tipo == 'usuario'):
            user = CustomUser.objects.get(id=obj.idObjeto)
            if (user is not None):
                return user.username
        elif (obj.tipo == 'contenido'):
            contenido = Contenido.objects.get(id=obj.idObjeto)
            if (contenido is not None):
                return contenido.titulo
        elif (obj.tipo == 'detalleContenido'):
            detalle = DetalleContenido.objects.get(id=obj.idObjeto)
            if (detalle is not None):
                return detalle.descripcion
        return ''
    descripcion_denuncia.short_description = "Descripción"

    list_display = ('id', 'tipo', 'idObjeto', 'descripcion_denuncia', 'razon')
    list_filter = ('tipo', 'idObjeto')
    exclude = ['usuario_modificacion', 'fecha_modificacion']
    list_display_links = None

    def desestimar_denuncia(self, request, queryset):
        queryset.update(fecha_baja=timezone.now(), usuario_baja=request.user.username)
    desestimar_denuncia.short_description = "Desestimar"

    #TODO: No funca bien
    def borrar_denuncia(self, request, queryset):
        import pdb; pdb.set_trace()
        for obj in queryset:
            if (obj.tipo == 'usuario'):
                perfil = Perfil.objects.get(id=obj.idObjeto)
                perfil.fecha_baja = timezone.now()
                perfil.usuario_baja = request.user.username
                user = perfil.usuario
                user.is_active = False
                perfil.save()
                user.save()
            elif (obj.tipo == 'contenido'):
                contenido = Contenido.objects.get(id=obj.idObjeto)
                contenido.fecha_baja = timezone.now()
                contenido.usuario_baja = request.user.username
                #Baja a detalles conectidos asociados.
                contenido.save()
            elif (obj.tipo == 'detalleContenido'):
                detalleContenido = DetalleContenido.objects.get(id=obj.idObjeto)
                detalleContenido.fecha_baja = timezone.now()
                detalleContenido.usuario_baja = request.user.username
                detalleContenido.save()
        queryset.update(fecha_baja=timezone.now(), usuario_baja=request.user.username)
    borrar_denuncia.short_description = "Eliminar"

    actions = ['desestimar_denuncia', 'borrar_denuncia']

    #TODO: Modificar Queryset. Group by id?
    def get_queryset(self, request):
        qs = super(AdminDenuncia, self).get_queryset(request)
        return qs.filter(fecha_baja__isnull=True)

    #No se puede agregar nada por django admin
    def has_add_permission(self, request, **kwargs):
        # No deja crear denuncias
        return False

    #Solo admins (?)
    def has_change_permission(self, request, obj=None, **kwargs):
        # Si el objeto es none => true es la lista.
        if (obj is None):
            if (request.user.is_admin):
                return True
            return False
        else:
            return False

    def has_delete_permission(self, request, **kwargs):
        # No deja crear denuncias
        return False


# Register your models here.
# Common admin
#admin.site.register(Comunidad, AdminComunidad)
#admin.site.register(Perfil, AdminPerfil)
#admin.site.register(Denuncia, AdminDenuncia)

# Custom admin site
admin_site.register(Comunidad, AdminComunidad)
admin_site.register(Perfil, AdminPerfil)
admin_site.register(Denuncia, AdminDenuncia)

# Registrar el custom user o no. Según lo que queramos.
#admin.site.register(CustomUser, CustomUserAdmin)
# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.
#from django.contrib.auth.models import Group
#admin.site.unregister(Group)