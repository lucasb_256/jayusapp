# -*- coding: utf-8 -*-
from haystack import indexes
from JayusApp.models import Perfil, Contenido
from JayusApp.Common.Enum import VisibilidadEnum
from JayusApp.Common.Enum import EstadoContenidoEnum, EstadoPerfilEnum

class PerfilIndex(indexes.SearchIndex,indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        return Perfil

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(estado=EstadoPerfilEnum.Activo.value)
        EstadoPerfilEnum.Activo.value

class ContenidoIndex(indexes.SearchIndex,indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    puntuacion = indexes.EdgeNgramField(model_attr="puntuacion")
    estado = indexes.EdgeNgramField(model_attr="estado")

    def get_model(self):
        return Contenido

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(visibilidad=VisibilidadEnum.Publico.value, estado=EstadoContenidoEnum.Activo.value)

#class CustomUserIndex(indexes.SearchIndex, indexes.Indexable):
    #text = indexes.CharField(document=True, use_template=True)

    #def get_model(self):
        #return CustomUser

    #def index_queryset(self, using=None):
        #return self.get_model().objects.filter(is_active=True)

#class DetalleContenidoIndex(indexes.SearchIndex,indexes.Indexable):
    #text = indexes.CharField(document=True, use_template=True)

    #def get_model(self):
        #return DetalleContenido

    #def index_queryset(self, using=None):
        #return self.get_model().objects.filter(tipo_contenido=TipoContenidoEnum.Comentario.value)


