# -*- coding: utf-8 -*-
import json
import itertools
#Django
from django.core.mail import send_mail
from django.utils import timezone
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.template import RequestContext
from django.shortcuts import render, render_to_response
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.models import Group
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
##JayusApp
from JayusApp.Common.Enum import VisibilidadEnum
#Models
from JayusApp.models import RegistrosPendientes, Contenido, DetalleContenido
from JayusApp.models import Perfil, Comunidad, Miembro, Denuncia, CustomUser
#Forms
from JayusApp.forms.userForm import UserCreationForm, RegistrosPendientesForm
from JayusApp.forms.contenidoForm import ContenidoForm
from JayusApp.forms.commentForm import NewCommentForm
from JayusApp.forms.perfilForm import PerfilForm
from JayusApp.forms.comunidadForm import ComunidadForm, MiembroForm
#Consultas
from JayusApp.consultas.consultasComunidad import comunidad_por_user, esLider
from JayusApp.consultas.consultasComunidad import esMiembro, esModerador
from JayusApp.consultas.consultasComunidad import get_comunidad_miembros
from JayusApp.consultas.consultasComunidad import obtenerUser, obtenerRol
from JayusApp.consultas.consultasComunidad import editMiembros, nombre_repetido
from JayusApp.consultas.consultasComunidad import get_miembro, obtenerRolPorId
from JayusApp.consultas.consultasPerfil import ObtenerPerfil
from JayusApp.consultas.consultasPerfil import ObtenerPerfilPorUsername
from JayusApp.consultas.consultasContenido import cantPuntosUsuarioContenido
from JayusApp.consultas.consultasSeguridad import puede_eliminar_contenido
# Create your views here.


def home(request):
    if not request.user.is_authenticated():
        content_list = Contenido.objects.filter(visibilidad=VisibilidadEnum.Publico.value, fecha_baja__isnull=True).order_by('-fecha_modificacion')
    else:
        content_list = Contenido.objects.exclude(fecha_baja__isnull=False).order_by('-fecha_modificacion')
    return render(request, 'home.html', {'content_list': content_list})


def gracias(request):
    return render(request, 'gracias.html')


def aboutUs(request):
    return render(request, 'aboutUs.html')


def registrar(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            username = form['username'].value()
            email = form['email'].value()
            usuario = form.save()

            data = {'usuario_modificacion': usuario.id,
                'es_valido': True}
            user_validation = RegistrosPendientesForm(data)
            user_validation.is_valid()
            a_validar = user_validation.save()

            subject = 'Alta usuario'
            message = "Bienvenido a Jayus. \n Gracias por registrarte " + username + ". \n "
            message += "Para validar, por favor ingrese al siguiente link: "
            message += "http://" + request.get_host() + "/validar/" + str(a_validar.token)
            sender = 'JayusApp@hotmail.com'
            recipients = [email]

            send_mail(subject, message, sender, recipients, fail_silently=False)
            return HttpResponseRedirect('/gracias')
        else:
            return render(request, 'registrar_user.html', {'form': form})
    else:
        if request.user.is_authenticated():
            return HttpResponseRedirect('/home')
        else:
            form = UserCreationForm()
            return render(request, 'registrar_user.html', {'form': form})


def validar(request, id):
    if request.method == 'POST':
        if (id != ''):
            user = RegistrosPendientes.objects.filter(token = id).first().usuario_modificacion
            user.is_active = True
            #user.is_staff = True
            user.is_admin = False
            user.save()

            #Crear perfil
            perfil = Perfil()
            perfil.usuario = user
            perfil.imagen = "/static/img/users.png"
            perfil.save()

            #g = Group.objects.get(name='Novato')
            #g.perfil_set.add(perfil)
            # Para pruebas
            #user = RegistrosPendientes.objects.all().first()
            #perfil = ObtenerPerfil(user, request.get_host())
            #return HttpResponseRedirect('/home')
            #return HttpResponseRedirect('/perfil/' + str(perfil.id))
            return HttpResponseRedirect('/perfil/edit/' + str(perfil.id))
        else:
            raise Http404
    else:
        context = {'id': id}
        return render(request, 'validar_user.html', context)


# TODO: Si no está logeado que vea lo público del chabon.
@login_required
def perfilView(request, idPerfil=''):
    if (idPerfil != ''):
        query = Perfil.objects.filter(id=idPerfil).first()
        if (query is not None):
            contenido = Contenido.objects.filter(usuario=query.usuario).order_by('-fecha_modificacion')
            contenido = contenido.filter(fecha_baja__isnull=True)
            form = {
                'idPerfil': idPerfil,
                'perfil': query,
                # Si administrator in Perfil.groups=>deja borrar
                'usuario': query.usuario,
                'content_list': contenido
            }
            return render(request, 'perfil.html', form)
        else:
            raise Http404
    else:
        query = ObtenerPerfil(request.user, request.get_host())
        contenido = Contenido.objects.filter(usuario=request.user.id)
        contenido = contenido.filter(fecha_baja__isnull=True)
        form = {
            'idPerfil': query.id,
            'perfil': query,
            'usuario': request.user,
            'content_list': contenido
        }
        return render(request, 'perfil.html', form)


def redirectPerfil(request):
    # Tiene 3 parametros:
    #    preurl (por si queremos redirigir a algun lugar particular)
    #    idPerfil
    #    username
    #    (Sino viene idperfil o username, devuelve al propio)
    # Prueba obtener y sino devuelve None.
    pre_url = request.GET.get('pre_url', None)
    if (pre_url is None):
        pre_url = '/perfil/'

    idPerfil = request.GET.get('idPerfil', None)
    if (idPerfil is not None):
        return HttpResponseRedirect(pre_url + str(idPerfil))
    else:
        username = request.GET.get('username', None)
        if (username is not None):
            user = CustomUser.objects.get(username=username)
            if (user is not None):
                perfil = ObtenerPerfilPorUsername(user, request.get_host())
                return HttpResponseRedirect(pre_url + str(perfil.id))

    perfil = ObtenerPerfil(request.user, request.get_host())
    return HttpResponseRedirect(pre_url + str(perfil.id))


@login_required
def editPerfil(request):
    if request.method == 'POST':
        perfil = ObtenerPerfil(request.user, request.get_host())
        form = PerfilForm(request.POST, request.FILES, instance=perfil)
        if (form.is_valid()):
            form.save()
            return HttpResponseRedirect("/perfil/")
        return render(request, 'edit_perfil.html', {'perfil': form})
    perfil = ObtenerPerfil(request.user, request.get_host())
    if (perfil.sexo is None):
        perfil.sexo = 0
    form = {
        'perfil': PerfilForm(instance=perfil)
        }
    return render(request, 'edit_perfil.html', form)


@login_required
def mis_comunidades(request):
    form = {
        'mis_comunidades': comunidad_por_user(request.user)
    }
    return render(request, 'mis_comunidades.html', form)


@login_required
def comunidad_miembros(request, idComunidad):
    comunidad = Comunidad.objects.filter(id=idComunidad).first()
    perfil = ObtenerPerfil(request.user, request.get_host())

    if comunidad is not None:
        #if (esMiembro(comunidad, perfil)):
        miembros = get_comunidad_miembros(comunidad)
        perfil_list = Perfil.objects.filter(usuario__in=miembros)

        form = {
            'nombreComunidad': Comunidad.objects.filter(id=idComunidad).first().nombre,
            'idComunidad': idComunidad,
            'esLider': esLider(comunidad, perfil),
            'perfil_list': perfil_list
        }
        return render(request, 'comunidad_miembros.html', form)
    raise Http404


#TODO: Funciona? Revisar mejor.
@login_required
def eliminar_miembro(request, idPerfil, idComunidad):
    comunidad = Comunidad.objects.filter(id=idComunidad).first()
    perfil = Perfil.objects.get(id=idPerfil)

    if (perfil is not None):
        if comunidad is not None:
            if (esMiembro(comunidad, perfil)):
                perfil_lider = ObtenerPerfil(request.user, request.get_host())
                if (esLider(comunidad, perfil_lider)):
                    m = Miembro.objects.get(comunidad_id=comunidad.id,
                        usuario_id=perfil.usuario.id)
                    m.fecha_baja = timezone.now()
                    m.usuario_baja = request.user.username
                    m.save()
    return HttpResponseRedirect('/comunidad/miembros/' + str(idComunidad))


@login_required
def comunidad(request, idComunidad):
    if (idComunidad != ''):
        query = Comunidad.objects.filter(id=idComunidad).first()
        if (query is not None):
            if Miembro.objects.filter(comunidad=idComunidad,
                    usuario=request.user.id):
                form = {
                    'idComunidad': idComunidad,
                    'comunidad': query,
                    'puede_adm': esLider(query, request.user),
                    'puede_comentar': esMiembro(query, request.user),
                    'content_list': Contenido.objects.filter(comunidad=idComunidad, fecha_baja__isnull=True),
                    }
                #import pdb; pdb.set_trace()
                return render(request, 'comunidad.html', form)
    raise Http404


@login_required
def comunidad_add(request):
    form = {
        'titulo': 'Crear comunidad',
        'comunidad': ComunidadForm(),
        'miembro': MiembroForm()
        }

    if request.method == 'POST':
        data = {
            'errorNombre': '',
            'errorDescripcion': '',
            'errorNombreRepetido': '',
            'comunidadGuardada': '',
            'todoGuardado': ''
            }
        titulo = request.POST.get('titulo', None)
        descripcion = request.POST.get('descripcion', None)
        miembros = request.POST.get('miembros', None)

        if (titulo):
            if (descripcion):
                if (nombre_repetido(titulo)):
                    data['errorNombreRepetido'] = "Debe ingresar nombre único"
                else:
                    nueva_comunidad = Comunidad()
                    nueva_comunidad.nombre = titulo
                    nueva_comunidad.descripcion = descripcion
                    nueva_comunidad.save()
                    data['comunidadGuardada'] = "Se guardó la comunidad, ha ocurrido un error en los miembros"
                    miembros = miembros.replace('"', '')
                    miembros = miembros.replace('[', '')
                    miembros = miembros.replace(']', '')
                    miembros = miembros.split(',')
                    for m in miembros:
                        usuario, rol = m.split(':')
                        nuevo_miembro = Miembro()
                        nuevo_miembro.comunidad = nueva_comunidad
                        nuevo_miembro.rol = obtenerRol(rol)
                        nuevo_miembro.usuario = obtenerUser(usuario)
                        nuevo_miembro.save()
                    data['comunidadGuardada'] = ''
                    data['todoGuardado'] = "Se guardó la comunidad"
            else:
                data['errorDescripcion'] = "Debe ingresar descripción"
        else:
            data['errorNombre'] = "Debe ingresar nombre"
        vuelta = json.dumps(data)
        return HttpResponse(vuelta, content_type='application/json')
        return HttpResponseRedirect('/mis_comunidades/')
    return render(request, 'comunidad_abm.html', form)


@login_required
def comunidad_edit(request, idComunidad):
    if (idComunidad is not None):
        comunidad = Comunidad.objects.get(id=idComunidad)
        miembros = []
        for m in get_miembro(comunidad):
            miembros.append((m.usuario.username, obtenerRolPorId(m.rol)))
        iterator = itertools.count()
        if (comunidad is not None):
            form = {
                'comunidadId': comunidad.id,
                'titulo': 'Editar comunidad: ',
                'comunidad': ComunidadForm(instance=comunidad),
                'miembros': miembros,
                'miembro': MiembroForm(),
                'iterator': iterator
            }

            if request.method == 'POST':
                data = {
                    'errorNombre': '',
                    'errorDescripcion': '',
                    'errorNombreRepetido': '',
                    'comunidadGuardada': '',
                    'todoGuardado': ''
                    }
                titulo = request.POST.get('titulo', None)
                descripcion = request.POST.get('descripcion', None)
                miembros = request.POST.get('miembros', None)
                # Pregunto si está vacio
                if (titulo):
                    if (descripcion):
                        if (titulo != form['comunidad']['nombre'].value()):
                            if (nombre_repetido(titulo)):
                                data['errorNombreRepetido'] = "Debe ingresar nombre único"
                        else:
                            comunidad.nombre = titulo
                            comunidad.descripcion = descripcion
                            comunidad.save()
                            data['comunidadGuardada'] = "Se guardó la comunidad, ha ocurrido un error en los miembros"
                            miembros = miembros.replace('"', '')
                            miembros = miembros.replace('[', '')
                            miembros = miembros.replace(']', '')
                            miembros = miembros.split(',')

                            editMiembros(comunidad, form['miembros'], miembros, request.user)
                            data['comunidadGuardada'] = ''
                            data['todoGuardado'] = "Se guardó la comunidad"
                    else:
                        data['errorDescripcion'] = "Debe ingresar descripción"
                else:
                    data['errorNombre'] = "Debe ingresar nombre"
                vuelta = json.dumps(data)
                return HttpResponse(vuelta, content_type='application/json')
            return render(request, 'comunidad_abm.html', form)
    return Http404


@login_required
def comunidad_delete(request, idComunidad):
    if (idComunidad is not None):
        comunidad = Comunidad.objects.get(id=idComunidad)
        if (request.user.is_admin or esLider(comunidad, request.user)):
            comunidad.fecha_baja = timezone.now()
            comunidad.usuario_baja = request.user.username
            comunidad.save()
    return HttpResponseRedirect('/mis_comunidades/')


@login_required
def alta_comentario(request, idContenido):
    if request.method == 'POST':
        if (idContenido is not None):
            form = NewCommentForm(request.POST)
            if form.is_valid():
                obj = DetalleContenido()
                obj.contenido = Contenido.objects.filter(id=idContenido).first()
                obj.descripcion = form['descripcion'].value()
                obj.usuario = request.user
                obj.usuario_modificacion = request.user
                obj.save()
                return HttpResponseRedirect('/contenido/' + str(idContenido))
    return render(request, 'alta_comentario.html', { 'idContenido' : idContenido})


@login_required
def contenido(request, idContenido):
    if (idContenido != 0):
        contenido = Contenido.objects.filter(id=idContenido).first()
        if (contenido is not None):
            if (contenido.fecha_baja is None):
                comentarios = DetalleContenido.objects.filter(contenido=idContenido,
                    tipo_contenido=1, fecha_baja__isnull=True)

                perfil = ObtenerPerfilPorUsername(contenido.usuario_modificacion, request.get_host())
                form = {
                    'contenido': contenido,
                    'formContenido': NewCommentForm,
                    'comentarios': comentarios,
                    #TODO: Hacer andar, f(x) si puede_eliminar (admin, propio,
                    #        moderador_comunidad, lider_comunidad)
                    'puede_eliminar': puede_eliminar_contenido(request.user, contenido),
                    'cant_comentarios': comentarios.count(),
                    'perfil_autor': perfil
                }
                return render(request, 'contenido.html', form)
    raise Http404


@login_required
def contenido_sin_params(request):
    perfil = ObtenerPerfil(request.user, request.get_host())
    return HttpResponseRedirect('/contenido/add/perfil/' + str(perfil.id))


# Tipo: Perfil o Comunidad
@login_required
def contenido_add(request, lugar='', idLugar=''):
    if request.method == 'POST':
        form = ContenidoForm(request.POST)
        if form.is_valid():
            obj = Contenido()
            obj.titulo = form['titulo'].value()
            obj.contenido = form['contenido'].value()
            obj.visibilidad = form['visibilidad'].value()
            obj.usuario_modificacion = request.user
            if (lugar == 'perfil'):
                obj.usuario = request.user
                obj.save()
                return HttpResponseRedirect('/perfil')
            else:
                obj.comunidad = Comunidad.objects.filter(id=idLugar).first()
                obj.save()
                return HttpResponseRedirect('/comunidad/' + str(idLugar))
    # Si no era post entonces ver q devuelvo según parámetros
    if (lugar is not None):
        if (lugar == 'perfil' or idLugar is None):
            obj = Perfil.objects.filter(usuario=request.user.id).first()
            if (obj is None):
                #Crear perfil si no existe
                perfil = Perfil()
                perfil.usuario = request.user
                perfil.save()

            context = {
                'form': ContenidoForm(),
                'lugar': lugar,
                'nombre': obj.usuario.username,
                'idLugar': idLugar
                }
            return render(request, 'contenido_add.html', context)

        elif (lugar == 'comunidad' and idLugar is not None):
            obj = Comunidad.objects.filter(id=idLugar).first()
            if (obj is not None):
                context = {
                'form': ContenidoForm(),
                'lugar': lugar,
                'nombre': obj.nombre,
                'idLugar': idLugar
                }
            return render(request, 'contenido_add.html', context)
    raise Http404


#TODO: Validar (User admin/propio/moderador) ¿Funciona?
@login_required
def eliminar_contenido(request, idContenido):
    perfil_lider = ObtenerPerfil(request.user, request.get_host())
    contenido = Contenido.objects.filter(id=idContenido).first()

    if (contenido is not None):
        if (contenido.comunidad):
            comunidad = Comunidad.objets.get(id=contenido.comunidad)
            if (comunidad is not None):
                if (esLider(comunidad, perfil_lider) or
                esModerador(comunidad, perfil_lider)):
                    contenido.fecha_baja = timezone.now()
                    contenido.usuario_baja = request.user.username
                    contenido.save()
                    return HttpResponseRedirect('/comunidad/' +
                        str(contenido.comunidad))
        elif (contenido.usuario):
            if (contenido.usuario == request.user):
                contenido.fecha_baja = timezone.now()
                contenido.usuario_baja = request.user.username
                contenido.save()
                return HttpResponseRedirect('/perfil')
            elif (request.user.is_admin):
                contenido.fecha_baja = timezone.now()
                contenido.usuario_baja = request.user.username
                contenido.save()
                perfil = ObtenerPerfil(request.user, request.get_host())
                return HttpResponseRedirect('/perfil/' + str(perfil.id))
    raise Http404


@login_required
def eliminar_comentario(request, idDetalleContenido, idContenido):
    # TODO: probar seguridad
    c = Contenido.objects.get(id=idContenido)
    comentario = DetalleContenido.objects.filter(id=idDetalleContenido).first()
    if (comentario.usuario == request.user):
        b = True
    if (c is not None):
        if (c.comunidad is not None):
            # Si es de comunidad => solo si es lider o moderador
            if (esLider(c.comunidad, request.user) or
            esModerador(c.comunidad, request.user)):
                b = True
        elif (c.usuario is not None):
            # Si es en perfil => solo si es admin
            if (request.user.is_admin):
                b = True
    if (b):
        comentario.fecha_baja = timezone.now()
        comentario.usuario_baja = request.user.username
        comentario.save()
    return HttpResponseRedirect('/contenido/' + str(idContenido) + '.html')


# todo: Ver bien csrf token. Test bien. Enganchar. Action. Lista denuncias (Admin).
@login_required
@csrf_exempt
def denunciar(request):
    datos = request.POST

    tipo = datos['tipo']
    idObjeto = datos['idObjeto']
    razon = datos['razon']

    # Si existe => entonces no hace nada pero todo ok.
    # Sino => si están los datos mínimos entonces guardo.
    # Si no pasa nada de eso, entonces todo mal.
    existe = Denuncia.objects.filter(tipo=tipo, idObjeto=idObjeto,
        usuario_modificacion=request.user.username)

    if existe.count() < 1:
        if (tipo == 'contenido' or tipo == 'detalleContenido' or tipo == 'usuario'):
            if (idObjeto is not None):
                denuncia = Denuncia()
                denuncia.tipo = tipo
                denuncia.idObjeto = idObjeto
                denuncia.razon = razon
                denuncia.fecha_modificacion = timezone.now()
                denuncia.usuario_modificacion = request.user
                denuncia.save()
                return HttpResponse('Todo Ok')
    else:
        denuncia = existe.first()
        denuncia.razon = razon
        denuncia.save()
        return HttpResponse('Todo Ok')
    return Http404


@login_required
def valorar_contenido(request, idContenido, p_tipo_contenido):
    if (idContenido is not None):
        el_usuario_puntuo_si_no = cantPuntosUsuarioContenido(request.user, idContenido)
        if (el_usuario_puntuo_si_no == 0):
            contenido1 = Contenido.objects.filter(id=idContenido).first()

            #agrega una valoración positiva al contenido
            obj = DetalleContenido()
            obj.contenido = contenido1
            obj.descripcion = "up"
            obj.usuario = request.user
            obj.usuario_modificacion = request.user
            obj.tipo_contenido = p_tipo_contenido
            obj.save()

            #calcular puntuación del contenido
            v_positivas = DetalleContenido.objects.filter(contenido=idContenido,
                tipo_contenido=2).count()
            v_negativas = DetalleContenido.objects.filter(contenido=idContenido,
                tipo_contenido=3).count()

            puntos = v_positivas - v_negativas
            contenido1.puntuacion = puntos
            contenido1.save()

            #calcular la puntuación del usuario
            punto = 0
            if (p_tipo_contenido == '2'):
                punto = 1
            elif (p_tipo_contenido == '3'):
                punto = -1

            perfil = ObtenerPerfilPorUsername(contenido1.usuario_modificacion, request.get_host())
            perfil.puntuacion = perfil.puntuacion + punto
            perfil.save()
    return HttpResponseRedirect('/contenido/' + str(idContenido))


def autor(request, idContenido):
    if (idContenido is not None):
        return HttpResponseRedirect('/contenido/' + str(idContenido))


#Para que cuando desloguee vaya al home
def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/home/')


#Devolver el custom 404
def error404(request):
    return render('404.html')


#Devolver el custom 500
def error500(request):
    return render('500.html')


@login_required
def modificar_contenido(request, idContenido):
    if request.method == 'POST':
        form = ContenidoForm(request.POST)
        obj = Contenido.objects.filter(id=idContenido).first()

        if form.is_valid():
            obj.titulo = form['titulo'].value()
            obj.contenido = form['contenido'].value()
            obj.visibilidad = form['visibilidad'].value()
            obj.save()
            return HttpResponseRedirect('/contenido/' + str(idContenido))
    # Si no era post entonces llamo a contenido_mod para editar
    else:
        #busco el contenido según el id
        obj = Contenido.objects.filter(id=idContenido).first()

        #cargo el formulario de contenido con la consulta
        form = ContenidoForm(initial={
            'titulo': obj.titulo,
            'contenido': obj.contenido,
            'visibilidad': obj.visibilidad
            }
        )

        context = {
            'idContenido': idContenido,
            'form': form,
            'nombre': obj.usuario_modificacion,
        }

        return render(request, 'contenido_mod.html', context)

    raise Http404


#def buscar(request):
    #filtro = request.GET.get('filtro', None)
    #if (filtro is None):
        #return HttpResponseRedirect('home')
    #else:
        #return HttpResponseRedirect(

    #idPerfil = request.GET.get('idPerfil', None)
    #if (idPerfil is not None):
        #return HttpResponseRedirect(pre_url + str(idPerfil))
    #else:
        #username = request.GET.get('username', None)
        #if (username is not None):
            #perfil = ObtenerPerfilPorUsername(request.user, request.get_host())
            #return HttpResponseRedirect(pre_url + str(perfil.id))
        #else:
            #perfil = ObtenerPerfil(request.user, request.get_host())
            #return HttpResponseRedirect(pre_url + str(perfil.id))
    #
    #filtro = request.GET.get('filtro', None)
    #if (filtro is None):
        #pre_url = '/perfil/'
    #perfil = ObtenerPerfil(request.user)
    #return HttpResponseRedirect(pre_url + str(perfil.id))


def login_user(request):
    logout(request)
    username = password = ''
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/home/')
    return render_to_response('login.html', context_instance=RequestContext(request))