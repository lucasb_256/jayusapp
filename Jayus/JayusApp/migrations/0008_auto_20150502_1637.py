# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0007_auto_20150502_1633'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comunidad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(default=b'Test', max_length=80)),
                ('descripcion', models.CharField(max_length=200, null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='contenido',
            name='grupo',
        ),
        migrations.RemoveField(
            model_name='miembro',
            name='grupo',
        ),
        migrations.DeleteModel(
            name='Grupo',
        ),
        migrations.AddField(
            model_name='contenido',
            name='comunidad',
            field=models.ForeignKey(blank=True, to='JayusApp.Comunidad', null=True),
        ),
        migrations.AddField(
            model_name='miembro',
            name='comunidad',
            field=models.ForeignKey(default=1, to='JayusApp.Comunidad'),
            preserve_default=False,
        ),
    ]
