# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0023_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contenido',
            name='fecha_modificacion',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='denuncia',
            name='fecha_modificacion',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='detallecontenido',
            name='fecha_modificacion',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='miembro',
            name='fecha_modificacion',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
