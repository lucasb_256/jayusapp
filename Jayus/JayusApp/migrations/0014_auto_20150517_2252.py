# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0013_auto_20150506_1901'),
    ]

    operations = [
        migrations.AddField(
            model_name='contenido',
            name='puntuacion',
            field=models.DecimalField(default=50, max_digits=4, decimal_places=0),
        ),
        migrations.AddField(
            model_name='perfil',
            name='puntuacion',
            field=models.DecimalField(default=50, max_digits=4, decimal_places=0),
        ),
    ]
