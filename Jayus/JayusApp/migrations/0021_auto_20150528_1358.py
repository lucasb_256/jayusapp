# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0020_auto_20150528_0410'),
    ]

    operations = [
        migrations.AddField(
            model_name='denuncia',
            name='idObjeto',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='denuncia',
            name='razon',
            field=models.CharField(default='Default', max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='denuncia',
            name='tipo',
            field=models.CharField(default='contenido', max_length=50),
            preserve_default=False,
        ),
    ]
