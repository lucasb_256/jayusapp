# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0014_auto_20150517_2252'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='miembro',
            name='estado',
        ),
        migrations.AddField(
            model_name='perfil',
            name='estado',
            field=models.CharField(default=1, max_length=1, choices=[(b'1', b'Activo'), (b'3', b'Baja'), (b'2', b'Bloqueado'), (b'0', b'Pendiente')]),
        ),
    ]
