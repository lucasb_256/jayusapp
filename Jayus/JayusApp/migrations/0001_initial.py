# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Contenido',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=100)),
                ('contenido', models.CharField(max_length=500)),
                ('fecha_modificacion', models.DateTimeField(auto_now=True)),
                ('activo', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='CustomUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('username', models.CharField(unique=True, max_length=50, verbose_name=b'Nombre de usuario')),
                ('email', models.EmailField(max_length=254, verbose_name=b'Correo electr\xc3\xb3nico')),
                ('is_active', models.BooleanField(default=False)),
                ('is_admin', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Grupo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('descripcion', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Miembros',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('baneado', models.BooleanField(default=False)),
                ('activo', models.BooleanField(default=False)),
                ('fecha_modificacion', models.DateTimeField(auto_now=True)),
                ('grupo', models.ForeignKey(blank=True, to='JayusApp.Grupo', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Perfil',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100, null=True)),
                ('apellido', models.CharField(max_length=100, null=True)),
                ('fechaNacimiento', models.DateField(auto_now=True)),
                ('Sexo', models.CharField(blank=True, max_length=1, choices=[(b'F', b'Femenino'), (b'M', b'Masculino')])),
            ],
        ),
        migrations.CreateModel(
            name='Roles',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.AddField(
            model_name='miembros',
            name='rol',
            field=models.ForeignKey(blank=True, to='JayusApp.Roles', null=True),
        ),
        migrations.AddField(
            model_name='miembros',
            name='usuario',
            field=models.ForeignKey(blank=True, to='JayusApp.CustomUser', null=True),
        ),
        migrations.AddField(
            model_name='contenido',
            name='usuario',
            field=models.ForeignKey(blank=True, to='JayusApp.CustomUser', null=True),
        ),
    ]
