# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0026_auto_20150610_2307'),
    ]

    operations = [
        migrations.AlterField(
            model_name='perfil',
            name='imagen',
            field=models.ImageField(default=b'http://localhost:8000/static/img/users.png', upload_to=b'perfiles/%Y/%m/%d'),
        ),
    ]
