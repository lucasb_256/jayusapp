# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0019_auto_20150528_0306'),
    ]

    operations = [
        migrations.AddField(
            model_name='comunidad',
            name='usuario_baja',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='comunidad',
            name='nombre',
            field=models.CharField(max_length=80),
        ),
    ]
