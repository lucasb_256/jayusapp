# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0025_auto_20150602_1300'),
    ]

    operations = [
        migrations.AlterField(
            model_name='perfil',
            name='fechaNacimiento',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='perfil',
            name='sexo',
            field=models.CharField(blank=True, max_length=1, null=True, choices=[(b'1', b'Femenino'), (b'3', b'Masculino'), (b'2', b'Neutro'), (b'0', b'Seleccione')]),
        ),
    ]
