# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0024_auto_20150530_1857'),
    ]

    operations = [
        migrations.AddField(
            model_name='miembro',
            name='fecha_baja',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='miembro',
            name='usuario_baja',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
