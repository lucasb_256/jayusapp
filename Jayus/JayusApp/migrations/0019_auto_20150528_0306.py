# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0018_auto_20150526_2126'),
    ]

    operations = [
        migrations.CreateModel(
            name='Denuncia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fecha_modificacion', models.DateField(default=django.utils.timezone.now)),
                ('usuario_modificacion', models.CharField(max_length=100, null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='contenido',
            name='fecha_baja',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='contenido',
            name='usuario_baja',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
