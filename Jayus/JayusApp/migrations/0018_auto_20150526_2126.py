# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0017_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='detallecontenido',
            name='fecha_baja',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='detallecontenido',
            name='usuario_baja',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
