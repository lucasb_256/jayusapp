# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0021_auto_20150528_1358'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contenido',
            name='puntuacion',
            field=models.DecimalField(default=0, max_digits=4, decimal_places=0),
        ),
    ]
