# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0008_auto_20150502_1637'),
    ]

    operations = [
        migrations.AlterField(
            model_name='perfil',
            name='apellido',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='perfil',
            name='imagen',
            field=models.ImageField(default=b'http://localhost:8000/static/img/users.png', upload_to=b''),
        ),
        migrations.AlterField(
            model_name='perfil',
            name='nombre',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='perfil',
            name='sexo',
            field=models.CharField(blank=True, max_length=1, null=True, choices=[(b'1', b'Femenino'), (b'3', b'Masculino'), (b'2', b'Neutro')]),
        ),
    ]
