# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0010_miembro_rol'),
    ]

    operations = [
        migrations.AlterField(
            model_name='miembro',
            name='rol',
            field=models.CharField(default=1, max_length=1, choices=[(b'3', b'Lider'), (b'2', b'Moderador'), (b'1', b'Publicador')]),
        ),
    ]
