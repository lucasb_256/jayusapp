# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0015_auto_20150518_2247'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contenido',
            name='fecha_modificacion',
            field=models.DateField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='detallecontenido',
            name='fecha_modificacion',
            field=models.DateField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='miembro',
            name='fecha_modificacion',
            field=models.DateField(default=django.utils.timezone.now),
        ),
    ]
