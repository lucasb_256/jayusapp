# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0021_auto_20150528_1358'),
    ]

    operations = [
        migrations.AddField(
            model_name='denuncia',
            name='fecha_baja',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='denuncia',
            name='usuario_baja',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='denuncia',
            name='razon',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
