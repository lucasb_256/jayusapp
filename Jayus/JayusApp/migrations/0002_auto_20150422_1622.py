# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='RegistrosPendientes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('token', models.CharField(default=uuid.uuid1, max_length=64, verbose_name=b'Activation key')),
                ('fecha_generacion', models.DateField(auto_now=True)),
                ('es_valido', models.BooleanField(default=False)),
            ],
        ),
        migrations.AlterField(
            model_name='customuser',
            name='email',
            field=models.EmailField(max_length=100, verbose_name=b'Correo electr\xc3\xb3nico'),
        ),
        migrations.AddField(
            model_name='registrospendientes',
            name='usuario',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
