# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0002_auto_20150422_1622'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registrospendientes',
            name='es_valido',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='registrospendientes',
            name='usuario',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
