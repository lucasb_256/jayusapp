# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0006_auto_20150501_1427'),
    ]

    operations = [
        migrations.CreateModel(
            name='Miembro',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fecha_modificacion', models.DateField(default=django.utils.timezone.now)),
                ('usuario_modificacion', models.CharField(max_length=100, null=True, blank=True)),
                ('estado', models.CharField(default=1, max_length=1, choices=[(b'1', b'Activo'), (b'3', b'Baja'), (b'2', b'Bloqueado'), (b'0', b'Pendiente')])),
                ('grupo', models.ForeignKey(to='JayusApp.Grupo')),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='miembros',
            name='grupo',
        ),
        migrations.RemoveField(
            model_name='miembros',
            name='usuario',
        ),
        migrations.DeleteModel(
            name='Miembros',
        ),
    ]
