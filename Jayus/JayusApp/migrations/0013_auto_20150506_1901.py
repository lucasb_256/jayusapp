# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        ('JayusApp', '0012_auto_20150506_1857'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customuser',
            name='groups',
        ),
        migrations.AddField(
            model_name='perfil',
            name='groups',
            field=models.ForeignKey(blank=True, to='auth.Group', null=True),
        ),
    ]
