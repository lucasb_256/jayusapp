# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0005_auto_20150430_2317'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='detallehistoricocontenido',
            name='usuario',
        ),
        migrations.DeleteModel(
            name='DetalleHistoricoContenido',
        ),
    ]
