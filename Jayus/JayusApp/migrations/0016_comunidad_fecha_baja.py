# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0015_auto_20150519_2056'),
    ]

    operations = [
        migrations.AddField(
            model_name='comunidad',
            name='fecha_baja',
            field=models.DateField(null=True, blank=True),
        ),
    ]
