# -*- coding: utf-8 -*-
#import datetime
from haystack import indexes
from JayusApp.models import Perfil

# ejemplo con nota
class PerfilIndex(indexes.SearchIndex, indexes.Indexable):
    usuario = indexes.CharField(model_attr='usuario', use_template=True)
    nombre = indexes.CharField(model_attr='nombre', use_template=True)
    apellido = indexes.CharField(model_attr='apellido', use_template=True)
    localidad = indexes.CharField(model_attr='localidad', use_template=True)

    def get_model(self):
        return Perfil

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(nombre__in='')