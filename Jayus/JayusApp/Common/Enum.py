# -*- coding: utf-8 -*-
from JayusApp.Common.utils import ChoiceEnum


class SexoEnum(ChoiceEnum):
    Seleccione = 0
    Femenino = 1
    Neutro = 2
    Masculino = 3


class VisibilidadEnum(ChoiceEnum):
    Publico = 1
    Privado = 2


class TipoContenidoEnum(ChoiceEnum):
    Comentario = 1
    Positivo = 2
    Negativo = 3


class EstadoContenidoEnum(ChoiceEnum):
    Activo = 1
    Eliminado = 2
    Editado = 3


class EstadoPerfilEnum(ChoiceEnum):
    Pendiente = 0
    Activo = 1
    Bloqueado = 2
    Baja = 3


class RolMiembroEnum(ChoiceEnum):
    # Dentro de una comunidad uno puede ser moderador, publicado o lider
    #Novato = 0
    Publicador = 1
    Moderador = 2
    Lider = 3
    #Administrador = 4