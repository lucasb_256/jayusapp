# -*- coding: utf-8 -*-
from django import forms
from JayusApp.models import Perfil
from JayusApp.Common.Enum import SexoEnum


class PerfilForm(forms.ModelForm):
    nombre = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    apellido  = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    localidad = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    fechaNacimiento = forms.DateField(required=False,
            widget=forms.TextInput(attrs={'class': 'form-control datepicker'}))
    sexo = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}),
            choices=SexoEnum.choices(), required=False)
    provincia = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    pais = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    telefono = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    usuario = forms.HiddenInput()
    puntuacion = forms.HiddenInput()

    class Meta:
        model = Perfil
        fields = ('usuario', 'imagen', 'nombre', 'apellido', 'fechaNacimiento',
                'sexo', 'localidad', 'provincia', 'pais', 'telefono', 'puntuacion')

    def __init__(self, *args, **kwargs):
        super(PerfilForm, self).__init__(*args, **kwargs)
        # assign a (computed, I assume) default value to the choice field
        self.initial['sexo'] = 'Seleccione un valor'
        # you should NOT do this:
        #self.fields['sexo'].initial = 'Seleccione un valor'