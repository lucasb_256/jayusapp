# -*- coding: utf-8 -*-
from django import forms
from JayusApp.models import Miembro, Comunidad, CustomUser
from JayusApp.Common.Enum import RolMiembroEnum

class MiembroForm(forms.ModelForm):
    usuario = forms.ModelChoiceField(required=True,
        widget=forms.Select(attrs={'class': 'form-control'}),
        queryset=CustomUser.objects.filter(is_active=True))
    rol = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}),
            choices=RolMiembroEnum.choices(), required=True)

    class Meta:
        model = Miembro
        fields = ('usuario', 'rol')

class ComunidadForm(forms.ModelForm):
    nombre = forms.CharField(required=True,
        widget=forms.TextInput(attrs={'class': 'form-control'}))
    descripcion = forms.CharField(required=True,
        widget=forms.Textarea(attrs={'class': 'form-control',
                                    'cols': 10, 'rows': 4}))

    class Meta:
        model = Comunidad
        fields = ('id', 'nombre', 'descripcion')