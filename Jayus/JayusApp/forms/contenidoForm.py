# -*- coding: utf-8 -*-
from django import forms
from JayusApp.models import Contenido
from ckeditor.widgets import CKEditorWidget


class ContenidoForm(forms.ModelForm):
    titulo = forms.CharField(label='Título', max_length=100,
                        required=True)
    contenido = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Contenido
        fields = ('titulo', 'contenido', 'visibilidad')
