# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.admin import UserAdmin
from JayusApp.models import CustomUser, RegistrosPendientes


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Contraseña', required=True,
                        widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmación de contraseña',
                        required=True, widget=forms.PasswordInput)
    username = forms.CharField(label='Nombre de usuario', max_length=50,
                        required=True)
    email = forms.EmailField(label='Correo Electrónico', max_length=100,
                        required=True)

    class Meta:
        model = CustomUser
        fields = ('username', 'email')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Las contraseñas no coinciden")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

    def validar(self, user):
        user.isActive = True
        CustomUser.update()
        CustomUser.save()


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    #password = ReadOnlyPasswordHashField()

    class Meta:
        model = CustomUser
        fields = ('username', 'password', 'is_active', 'is_admin')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class CustomUserAdmin(UserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm
    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('username', 'email', 'is_admin', 'is_active')
    list_filter = ('is_admin', 'is_active')

    fieldsets = (
        (None, {'fields': ('username', 'email', 'is_active')}),
        #('Personal info', {'fields': ('email',)}),
        ('Permissions', {'fields': ('is_admin',)}),
    )

    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', 'password1', 'password2')}
        ),
    )

    search_fields = ('username', 'email')
    ordering = ('username', 'email')
    filter_horizontal = ()


class RegistrosPendientesForm(forms.ModelForm):
    usuario_modificacion = forms.ModelChoiceField(queryset=CustomUser.objects.all(),
                                        empty_label="Seleccionar")
    #fecha_generacion = forms.DateField()
    #token = forms.UUIDField(default=uuid.uuid1)
    es_valido = forms.BooleanField()

    class Meta:
        model = RegistrosPendientes
        fields = ('usuario_modificacion', 'es_valido')

