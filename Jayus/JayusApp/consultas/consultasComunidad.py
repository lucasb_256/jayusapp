# -*- coding: utf-8 -*-
from JayusApp.models import Miembro, Comunidad, CustomUser
from JayusApp.Common.Enum import RolMiembroEnum
from django.utils import timezone


def esMiembro(comunidadParam, userParam):
    return (Miembro.objects.filter(comunidad=comunidadParam.id,
            usuario=userParam.id, fecha_baja__isnull=True).count() > 0)


def esLider(comunidadParam, userParam):
    miembro = Miembro.objects.filter(comunidad=comunidadParam.id,
                    usuario=userParam.id, fecha_baja__isnull=True)
    if (miembro.count() > 0):
        rol = miembro.first().rol
        return (rol == str(RolMiembroEnum.Lider.value))
    else:
        return False


def esModerador(comunidadParam, userParam):
    miembro = Miembro.objects.filter(comunidad=comunidadParam.id,
                    usuario=userParam.id, fecha_baja__isnull=True)
    if (miembro.count() > 0):
        rol = miembro.first().rol
        return (rol == str(RolMiembroEnum.Moderador.value))
    else:
        return False


def obtener_por_user(userParam):
    return Miembro.objects.filter(usuario=userParam.id,
        fecha_baja__isnull=True).values('comunidad')


def obtener_por_user_lider(userParam):
    return Miembro.objects.filter(usuario=userParam.id,
        rol=str(RolMiembroEnum.Lider.value),
        fecha_baja__isnull=True).values('comunidad')


def comunidad_por_user(userParam):
    return Comunidad.objects.filter(pk__in=obtener_por_user(userParam),
                                    fecha_baja__isnull=True)


def get_comunidad_miembros(comunidadParam):
    return Miembro.objects.filter(comunidad=comunidadParam.id,
        fecha_baja__isnull=True).values('usuario')


def get_miembro(comunidadParam):
    return Miembro.objects.filter(comunidad=comunidadParam.id,
        fecha_baja__isnull=True)

def comunidad_donde_lider(userParam):
    return Comunidad.objects.filter(pk__in=obtener_por_user_lider(userParam))


def obtenerUser(usernameParam):
    return CustomUser.objects.get(username=usernameParam)


def obtenerRol(rolParam):
    for r in RolMiembroEnum.choices():
        if (r[1] == rolParam):
            return r[0]
    return None


def obtenerRolPorId(rolParam):
    for r in RolMiembroEnum.choices():
        if (r[0] == rolParam):
            return r[1]
    return None


def editMiembros(comunidad, miembros_viejos, miembros_nuevos, usuario_actual):
    for m in miembros_nuevos:
        username, rolNombre = m.split(':')
        rol = obtenerRol(rolNombre)
        Duplicados = []

        for mv in miembros_viejos:
            if (mv[0] == username):
                if (mv[1] == rolNombre):
                    Duplicados.append(mv[0])
                else:
                    m_actualizar = Miembro.objects.get(id=mv.id)
                    m_actualizar.rol = rol
                    m_actualizar.save()
                    Duplicados.append(mv[0])

    for mv in [x for x in miembros_viejos if (x[0] not in Duplicados)]:
        usuario = obtenerUser(mv[0])
        m_actualizar = Miembro.objects.get(usuario=usuario, comunidad=comunidad,
                                        fecha_baja__isnull=True)
        m_actualizar.fecha_baja = timezone.now()
        m_actualizar.usuario_baja = usuario_actual.username
        m_actualizar.save()

    for m in miembros_nuevos:
        username, rolNombre = m.split(':')
        usuario = obtenerUser(username)
        rol = obtenerRol(rolNombre)
        m_actual = Miembro.objects.filter(usuario=usuario, comunidad=comunidad).first()
        if (m_actual is None):
            #add
            #import pdb; pdb.set_trace()
            AgregarMiembro(comunidad, rol, usuario)
        else:
            m_actual.rol = rol
            m_actual.fecha_baja = None
            m_actual.save()


def AgregarMiembro(comunidad, rol, usuario):
    nuevo_miembro = Miembro()
    nuevo_miembro.comunidad = comunidad
    nuevo_miembro.rol = rol
    nuevo_miembro.usuario = usuario
    nuevo_miembro.save()


#Por cada miembro viejo
    #Por cada miembro nuevo
        #Si usuario viejo == usuario nuevo
            #Si rol viejo == rol nuevo
                #Duplicados(miembro)
            #Sino
                #Actualizar rol
                #Duplicados(miembro)

#Por cada miembro viejo
	#Si miembro viejo not in Duplicados:
		#Obtener
		#Fecha baja

#Por cada miembro nuevo
	#Si miembro nuevo not in Duplicados
		#Si !no esta dados de baja:
			#Agregar
		#Sino
			#Sacar fecha baja
			
			
def nombre_repetido(nombreParam):
        c = Comunidad.objects.filter(nombre=nombreParam)
        return (c.count() > 0)