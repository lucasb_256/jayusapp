# -*- coding: utf-8 -*-
from JayusApp.models import Perfil, CustomUser
from django.contrib.auth.models import Group

def ObtenerPerfil(user, url_base='seia-jayus.herokuapp.com'):
    if (user is not None):
        salida = Perfil.objects.filter(usuario=user.id).first()

        if (salida is None):
            perfil = Perfil()
            perfil.usuario = user
            perfil.imagen = "http://" + url_base + "/static/img/users.png"
            perfil.save()
            return perfil

        return salida

    return None


def ObtenerPerfilPorUsername(usernameParam, url_base='seia-jayus.herokuapp.com'):
    if (usernameParam is not None):
        user = CustomUser.objects.get(username=usernameParam)
        if (user is not None):
            salida = Perfil.objects.get(usuario=user.id)
            if (salida is None):
                perfil = Perfil()
                perfil.usuario = user
                perfil.imagen = "http://" + url_base + "/static/img/users.png"
                perfil.save()
                return perfil
            return salida
    return None


def HacerSuperUser(perfil, user):
    if perfil is None:
        perfil = ObtenerPerfil(user)

    if (perfil is not None):
        #Obtener el grupo
        g = Group.objects.get(name='Superuser')
        g.perfil_set.add(perfil)
        return True
    else:
        False