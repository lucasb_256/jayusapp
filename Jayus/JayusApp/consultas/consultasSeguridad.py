# -*- coding: utf-8 -*-
# Seguridad:
#    Normal: Editar perfil propio. Crear comunidad.
#    Miembro: (En comunidad)
#        - Entrar
#        - Postear
#    Moderador:
#        - Eliminar contenido
#    Lider:
#        - Eliminar usuarios
#    Superuser: (En sitio)
#        - Casi todo
#    Administrador: user.is_admin => de todo.


from JayusApp.models import Perfil, CustomUser, Comunidad
from django.contrib.auth.models import Group
from JayusApp.consultas.consultasPerfil import ObtenerPerfil
from JayusApp.consultas.consultasComunidad import esLider, esModerador


def EsAdministrador(user):
    return user.is_admin


def EsSuperUser(user):
    perfil = ObtenerPerfil(user)
    g = Group.objects.get(name='SuperUser')
    # Como se hará eso?
    #g.perfil_get(perfil)


#TODO: Revisar bien
def puede_eliminar_contenido(user, contenido):
    if (EsAdministrador(user)):
        return True
    if (contenido.usuario):
        #Si es el propio usuario se revisa en la vista pa evitar problemas
        return False
        #if (contenido.usuario == user):
            #return True
    elif (contenido.comunidad):
        comunidad = Comunidad.objects.get(id=contenido.comunidad.id)
        if (comunidad is not None):
            if (esLider(comunidad, user)):
                return True
            elif (esModerador(comunidad, user)):
                return True
    return False