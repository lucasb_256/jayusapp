# -*- coding: utf-8 -*-
from JayusApp.models import DetalleContenido

def cantPuntosUsuarioContenido(user,idContenido):
    cant_puntos=0
    if (user is not None):
        if (idContenido is not None):
            cant_puntos = DetalleContenido.objects.filter(contenido_id=idContenido, usuario_modificacion=user).exclude(tipo_contenido=1).count()
    return cant_puntos

