# -*- coding: utf-8 -*-
from django.conf.urls import include, url
#common admin
#from django.contrib import admin
#custom admin
from JayusApp.admin import admin_site
# Esta bueno pero mas comeplejo.
# from robots_txt.views import RobotsTextView
from django.http import HttpResponse
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    # Examples:
    # url(r'^blog/', include('blog.urls')),

    # admin
    url(r'^admin/', include(admin_site.urls)),
    #Admin site por defecto
    #url(r'^admin/', include(admin.site.urls)),
    #Login por defecto
    #url(r'^accounts/login/$', 'django.contrib.auth.views.login',
        #{'template_name': 'admin/login.html'}, name="my_login"),
    #Custom login
    url(r'^accounts/login/$', 'JayusApp.views.login_user'),
    # rich textbox
    url(r'^ckeditor/', include('ckeditor.urls')),
    # buscar
    url(r'^search/', include('haystack.urls')),
    #url(r'^buscar/get/', 'JayusApp.views.buscar'),

    # urls al home
    url(r'^$', 'JayusApp.views.home', name='home'),
    url(r'^/', 'JayusApp.views.home'),
    url(r'^home/', 'JayusApp.views.home'),
    url(r'^admin/logout/home/', 'JayusApp.views.logout_view'),

    # registrar/validar
    url(r'^registrar/', 'JayusApp.views.registrar'),
    url(r'^validar/(?P<id>[^/]+)', 'JayusApp.views.validar'),

    # funcionales
    url(r'^perfil/get/', 'JayusApp.views.redirectPerfil'),
    url(r'^perfil/edit', 'JayusApp.views.editPerfil'),
    url(r'^perfil/(?P<idPerfil>[^/]+)', 'JayusApp.views.perfilView'),
    url(r'^perfil/', 'JayusApp.views.perfilView'),
    url(r'^mis_comunidades/', 'JayusApp.views.mis_comunidades'),
    url(r'^comunidad/add', 'JayusApp.views.comunidad_add'),
    url(r'^comunidad/edit/(?P<idComunidad>[0-9]+)', 'JayusApp.views.comunidad_edit'),
    url(r'^comunidad/delete/(?P<idComunidad>[0-9]+)', 'JayusApp.views.comunidad_delete'),
    url(r'^comunidad/miembros/eliminar/(?P<idPerfil>[0-9]+)/(?P<idComunidad>[0-9]+)',
            'JayusApp.views.eliminar_miembro'),
    #TODO: Revisar. Los redirects más que nada
    url(r'^comunidad/miembros/(?P<idComunidad>[0-9]+)',
                'JayusApp.views.comunidad_miembros'),
    url(r'^comunidad/(?P<idComunidad>[^/]+)', 'JayusApp.views.comunidad'),
    #url(r'^robots.txt$', RobotsTextView.as_view()),
    #Bien sencillito (http://fredericiana.com/2010/06/09/three-ways-to-add-a-robots-txt-to-your-django-project/)
    url(r'^robots.txt$', lambda r: HttpResponse("User-agent: *\nDisallow: /", content_type="text/plain")),
    # statics
    url(r'^aboutUs/', 'JayusApp.views.aboutUs'),
    url(r'^gracias/', 'JayusApp.views.gracias'),

    # contenido
     url(r'^contenido/modificar/(?P<idContenido>[0-9]+)',
                'JayusApp.views.modificar_contenido'),
    url(r'^contenido/(?P<idContenido>[0-9]+)', 'JayusApp.views.contenido'),
    url(r'^contenido/eliminar/(?P<idContenido>[0-9]+)',
                'JayusApp.views.eliminar_contenido'),
    url(r'^comentario/eliminar/(?P<idDetalleContenido>[0-9]+)/(?P<idContenido>[0-9]+)',
                'JayusApp.views.eliminar_comentario'),
    #TODO: Revisar. Los redirects más que nada
    url(r'^contenido/add/(?P<lugar>\w+)/(?P<idLugar>\w+)',
                'JayusApp.views.contenido_add'),
    #TODO: Revisar. Los redirects más que nada
    url(r'^contenido/add/',
                'JayusApp.views.contenido_sin_params'),
    url(r'^denunciar/', 'JayusApp.views.denunciar'),
    #TODO: Revisar. Los redirects más que nada
    url(r'^comentario/add/(?P<idContenido>[0-9]+)',
                'JayusApp.views.alta_comentario'),
    #TODO: Revisar. Los redirects más que nada
    url(r'^contenido/valorar/(?P<idContenido>[0-9]+)/(?P<p_tipo_contenido>[0-9]+)',
                'JayusApp.views.valorar_contenido'),
    url(r'^autor/(?P<idContenido>[0-9]+)', 'JayusApp.views.autor'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

#Para manajar un 404 y un 500
handler404 = 'JayusApp.views.error404'
handler500 = 'JayusApp.views.error500'
