Django==1.8.1
Pillow==2.8.1
argparse==1.2.1
bootstrap-admin==0.3.6
dj-database-url==0.3.0
dj-static==0.0.6
django-ckeditor==4.4.7
django-extensions==1.5.3
django-toolbelt==0.0.1
enum34==1.0.4
gunicorn==19.3.0
psycopg2==2.6
six==1.9.0
static3==0.5.1
wsgiref==0.1.2
django-haystack==2.3.1
Whoosh==2.7.0
django-robots-txt==0.4
elasticsearch

